import setuptools


def get_version(rel_path):
    with open("./" + rel_path, 'r') as fp:
        for line in fp.readlines():
            if line.startswith('__version__'):
                delim = '"' if '"' in line else "'"
                return line.split(delim)[1]
        else:
            raise RuntimeError("Unable to find version string.")


with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="satellite",
    version=get_version('satellite/__init__.py'),
    author="Teemu Ikonen",
    author_email="tpikonen@mailbox.org",
    description="Navigation satellite (GPS/GNSS) application",
    license="GPL3",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://codeberg.org/tpikonen/satellite",
    platforms=["Linux", "Gnome"],
    project_urls={
        "Bug Tracker": "https://codeberg.org/tpikonen/satellite/issues",
    },
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: X11 Applications :: GTK",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Topic :: Desktop Environment :: Gnome",
        "Topic :: Scientific/Engineering :: GIS",
        "Topic :: Utilities",
    ],
    package_dir={"": "./"},
    packages=setuptools.find_packages(where="./"),
    python_requires=">=3.6",
    package_data={
        "": ["*.ui", "*.css"],
    },
    data_files=[
        ('share/applications', ['data/satellite.desktop']),
        ('share/icons/hicolor/scalable/apps',
            ['data/page.codeberg.tpikonen.satellite.svg'])],
    zip_safe=False,
    entry_points={
        "gui_scripts": "satellite=satellite.__main__:main",
    }
)
